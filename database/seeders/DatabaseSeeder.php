<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SocioSeeder::class);
        $this->call(PagoSeeder::class);
        $this->call(AsistenciaSeeder::class);
        $this->call(UserSeeder::class);
    }
}
