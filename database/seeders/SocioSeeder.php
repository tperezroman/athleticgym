<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class SocioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        
        DB::table('socio')->insert([
            'nombre' => 'Ezequiel',
            'apellido' => 'Yur',
            'DNI' => '11111111',
            'email' => 'yure'.'@gmail.com',
            'direccion' => 'Calle 9 esquina 108',
            'estado' => 'Vencido',
        ]);

        DB::table('socio')->insert([
            'nombre' => 'Cesar',
            'apellido' => 'Watson',
            'DNI' => '22222222',
            'email' => 'watson'.'@gmail.com',
            'direccion' => 'Calle 9 esquina 108',
            'estado' => 'Al dia',
        ]);

        DB::table('socio')->insert([
            'nombre' => 'Luciano',
            'apellido' => 'Bessoni',
            'dni' => '33333333',
            'email' => 'bessoni'.'@gmail.com',
            'direccion' => 'Calle 9 esquina 108',
            'estado' => 'Vencido',
        ]);

        DB::table('socio')->insert([
            'nombre' => 'Matias',
            'apellido' => 'Menazzi',
            'dni' => '44444444',
            'email' => 'menazzi'.'@gmail.com',
            'direccion' => 'Calle 9 esquina 108',
            'estado' => 'Vencido',
        ]);

        DB::table('socio')->insert([
            'nombre' => 'Mariano',
            'apellido' => 'Corazza',
            'dni' => '55555555',
            'email' => 'corazza'.'@gmail.com',
            'direccion' => 'Calle 9 esquina 108',
            'estado' => 'Al dia',
        ]);

        DB::table('socio')->insert([
            'nombre' => 'Martin',
            'apellido' => 'Perez Roman',
            'dni' => '6666666',
            'email' => 'perez'.'@gmail.com',
            'direccion' => 'Calle 9 esquina 108',
            'estado' => 'Vencido',
        ]);

        DB::table('socio')->insert([
            'nombre' => 'Julian',
            'apellido' => 'Rodriguez',
            'dni' => '38023477',
            'email' => 'jrp'.'@gmail.com',
            'direccion' => 'Calle 9 esquina 108',
            'estado' => 'Al dia',
        ]);

        DB::table('socio')->insert([
            'nombre' => 'Agustin',
            'apellido' => 'Torres',
            'dni' => '39932911',
            'email' => 'at'.'@gmail.com',
            'direccion' => 'Calle 9 esquina 108',
            'estado' => 'Al dia',
        ]);

        DB::table('socio')->insert([
            'nombre' => 'Juan',
            'apellido' => 'Fernandez',
            'dni' => '12345678',
            'email' => 'jf'.'@gmail.com',
            'direccion' => 'Calle 9 esquina 108',
            'estado' => 'Al dia',
        ]);

        DB::table('socio')->insert([
            'nombre' => 'Guido',
            'apellido' => 'Tebes',
            'dni' => '78723894',
            'email' => 'gt'.'@gmail.com',
            'direccion' => 'Calle 9 esquina 108',
            'estado' => 'Vencido',
        ]);

        DB::table('socio')->insert([
            'nombre' => 'Martin',
            'apellido' => 'Pereyra',
            'dni' => '41034562',
            'email' => 'pereyra.m'.'@gmail.com',
            'direccion' => 'Calle 9 esquina 108',
            'estado' => 'Vencido',
        ]);

        DB::table('socio')->insert([
            'nombre' => 'Armando Esteban',
            'apellido' => 'Quito',
            'dni' => '39942444',
            'email' => 'aeq'.'@gmail.com',
            'direccion' => 'Calle 9 esquina 108',
            'estado' => 'Vencido',
        ]);
    }
}
