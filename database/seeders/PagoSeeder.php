<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;


class PagoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        
        $fechaPago = Carbon::now();
        $fechaVencimiento = $fechaPago->addMonth();

        // $fechapago2 = Carbon::create(2021,1,25);

        DB::table('pago')->insert([
            'fkIdSocio'=> 1,
            'monto' => 1400,
            'fechaPago' => Carbon::create(2021,1,25),
            'fechaVencimiento' => Carbon::create(2021,1,25)->addMonth()
        ]);

        DB::table('pago')->insert([
            'fkIdSocio'=> 3,
            'monto' => 1400,
            'fechaPago' => Carbon::create(2021,1,19),
            'fechaVencimiento' => Carbon::create(2021,1,19)->addMonth()
        ]);

        DB::table('pago')->insert([
            'fkIdSocio'=> 2,
            'monto' => 1400,
            'fechaPago' => Carbon::now()->toDateTimeString(),
            'fechaVencimiento' => $fechaVencimiento
        ]);

        DB::table('pago')->insert([
            'fkIdSocio'=> 5,
            'monto' => 1400,
            'fechaPago' => Carbon::create(2021,2,10),
            'fechaVencimiento' => Carbon::create(2021,2,10)->addMonth()
        ]);
        DB::table('pago')->insert([
            'fkIdSocio'=> 5,
            'monto' => 1400,
            'fechaPago' => Carbon::now()->toDateTimeString(),
            'fechaVencimiento' => $fechaVencimiento
        ]);

        DB::table('pago')->insert([
            'fkIdSocio'=> 6,
            'monto' => 1400,
            'fechaPago' => Carbon::create(2021,3,9),
            'fechaVencimiento' => Carbon::create(2021,3,9)->addMonth()
        ]);
        DB::table('pago')->insert([
            'fkIdSocio'=> 6,
            'monto' => 1400,
            'fechaPago' => Carbon::create(2021,4,9),
            'fechaVencimiento' => Carbon::create(2021,4,9)->addMonth()
        ]);

        DB::table('pago')->insert([
            'fkIdSocio'=> 7,
            'monto' => 1400,
            'fechaPago' => Carbon::create(2021,3,12),
            'fechaVencimiento' => Carbon::create(2021,3,12)->addMonth()
        ]);
        DB::table('pago')->insert([
            'fkIdSocio'=> 7,
            'monto' => 1400,
            'fechaPago' => Carbon::now()->toDateTimeString(),
            'fechaVencimiento' => $fechaVencimiento
        ]);

        DB::table('pago')->insert([
            'fkIdSocio'=> 8,
            'monto' => 1400,
            'fechaPago' => Carbon::create(2021,1,12),
            'fechaVencimiento' => Carbon::create(2021,1,12)->addMonth()
        ]);
        DB::table('pago')->insert([
            'fkIdSocio'=> 8,
            'monto' => 1400,
            'fechaPago' => Carbon::now()->toDateTimeString(),
            'fechaVencimiento' => $fechaVencimiento
        ]);

        DB::table('pago')->insert([
            'fkIdSocio'=> 9,
            'monto' => 1400,
            'fechaPago' => Carbon::create(2021,5,12),
            'fechaVencimiento' => Carbon::create(2021,5,12)->addMonth()
        ]);
        DB::table('pago')->insert([
            'fkIdSocio'=> 9,
            'monto' => 1400,
            'fechaPago' => Carbon::now()->toDateTimeString(),
            'fechaVencimiento' => $fechaVencimiento
        ]);

        DB::table('pago')->insert([
            'fkIdSocio'=> 10,
            'monto' => 1400,
            'fechaPago' => Carbon::create(2021,1,2),
            'fechaVencimiento' => Carbon::create(2021,1,2)->addMonth()
        ]);

        DB::table('pago')->insert([
            'fkIdSocio'=> 11,
            'monto' => 1400,
            'fechaPago' => Carbon::create(2021,2,12),
            'fechaVencimiento' => Carbon::create(2021,2,12)->addMonth()
        ]);
        
        DB::table('pago')->insert([
            'fkIdSocio'=> 12,
            'monto' => 1400,
            'fechaPago' => Carbon::create(2021,2,8),
            'fechaVencimiento' => Carbon::create(2021,2,8)->addMonth()
        ]);

    }
}
