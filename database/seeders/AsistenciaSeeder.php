<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;


class AsistenciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        
        $fechaHoraAsistencia = Carbon::now()->timezone('America/Argentina/San_Luis');

        DB::table('asistencia')->insert([
            'fkIdSocio'=> 1,
            'fechaHoraAsistencia' => $fechaHoraAsistencia,
        ]);

        DB::table('asistencia')->insert([
            'fkIdSocio'=> 2,
            'fechaHoraAsistencia' => $fechaHoraAsistencia,
        ]);

        DB::table('asistencia')->insert([
            'fkIdSocio'=> 3,
            'fechaHoraAsistencia' => $fechaHoraAsistencia,
        ]);

        DB::table('asistencia')->insert([
            'fkIdSocio'=> 4,
            'fechaHoraAsistencia' => $fechaHoraAsistencia,
        ]);

        
    }
}
