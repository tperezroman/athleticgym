<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){

        Schema::create('pago',function(Blueprint $table){
            

            $table->unsignedBigInteger('fkIdSocio');
            $table  ->foreign('fkIdSocio')
                    ->references('idSocio')
                    ->on('socio')
                    ->constrained()
                    ->onDelete('cascade')
                    ->onUpdate('cascade');  
            $table->increments('idPago')->unique();
            $table->string('monto');
            $table->date('fechaPago');
            $table->date('fechaVencimiento');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        
        Schema::dropIfExists('pago');
    }
}
