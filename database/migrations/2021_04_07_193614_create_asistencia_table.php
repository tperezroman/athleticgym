<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAsistenciaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asistencia', function (Blueprint $table) {
            $table->unsignedBigInteger('fkIdSocio');
            $table  ->foreign('fkIdSocio')
                    ->references('idSocio')
                    ->on('socio')
                    ->constrained()
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->increments('idAsistencia')->unique();
            $table->datetime('fechaHoraAsistencia');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asistencia');
    }
}
