<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Pago;

class Socio extends Model
{
    protected $table = 'socio';
    protected $primaryKey = 'idSocio';
    protected $fillable = [
        'nombre',
        'apellido',
        'dni',
        'email',
        'direccion',
        'estado'
    ];

    public function pagos(){
        return $this->hasMany(Pago::class, 'fkIdSocio', 'idSocio');
    }

    public function scopeEstado($query){
        return $query->where('estado', 'Vencido');
    }

}
