<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Socio;

class Pago extends Model
{
    protected $table = 'pago';
    protected $primaryKey = 'idPago';
    protected $fillable = [
        'fkIdSocio',
        'monto',
        'fechaPago',
        'fechaVencimiento',
    ];

    public function socio(){
        return $this->belongsTo(Socio::class, 'fkIdSocio', 'idSocio');
    }
}
