<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Socio;

class Asistencia extends Model
{
    protected $table = 'asistencia';
    protected $primaryKey = 'idAsistencia';
    protected $fillable = [
        'fkIdSocio',
        'fechaHoraAsistencia',
    ];

    public function socio(){
        return $this->belongsTo(Socio::class, 'fkIdSocio', 'idSocio');
    }
}
