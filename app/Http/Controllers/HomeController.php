<?php

namespace App\Http\Controllers;

use App\Models\Socio;
use App\Models\Asistencia;
use App\Models\Pago;
use Illuminate\Http\Request;

class HomeController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $cantSocios = Socio::all()->count();
        $anio = now()->year;
        $mes = now()->month;
        $facturacionMes = Pago::whereYear('fechaPago', '=', $anio)->whereMonth('fechaPago', '=', $mes)->get()->sum('monto');
        $facturacionAnio = Pago::whereYear('fechaPago', '=', $anio)->get()->sum('monto');
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        $mes = $meses[$mes-1];
        $asistencias=Asistencia::orderBy('fechaHoraAsistencia','desc')->get();
        
        return view('dashboard', [  'cantSocios' => $cantSocios,
                                    'facturacionMes' => $facturacionMes,
                                    'facturacionAnio' => $facturacionAnio,
                                    'anio'=>$anio,
                                    'asistencias' => $asistencias,
                                    'mes'=>$mes
                                ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        //
    }
}
