<?php

namespace App\Http\Controllers;

use App\Models\Pago;
use App\Models\Socio;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PagoController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $pagos=Pago::orderBy('fechaPago','desc')->get();

        return view('pagos/index',['pagos' => $pagos] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($idSocio){
        $socio=Socio::find($idSocio);

        return view('pagos/create',['socio' => $socio]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $pago = new Pago;
        $pago->fkIdSocio=$request->fkIdSocio;
        $pago->monto=$request->monto;
        $pago->fechaPago=$request->fechaPago;
        $pago->fechaVencimiento=$request->fechaVencimiento;
        $pago->save();

        if($pago->fechaVencimiento > Carbon::now()){
            $pago->socio->estado="Al dia";
            $pago->socio->save();
        }

        return redirect(route('pagos.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pago  $pago
     * @return \Illuminate\Http\Response
     */
    public function show(Pago $pago){
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pago  $pago
     * @return \Illuminate\Http\Response
     */
    public function edit($idPago){
        $pago=Pago::find($idPago);

        return view('pagos/edit',['pago'=>$pago]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pago  $pago
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$idPago) {
        $pago = Pago::find($idPago);
        $pago->monto=$request->monto;
        $pago->fechaPago=$request->fechaPago;
        $pago->fechaVencimiento=$request->fechaVencimiento;
        $pago->save();

        if($pago->fechaVencimiento > Carbon::now()){
            $pago->socio->estado="Al dia";
            $pago->socio->save();
        }else{
            $pago->socio->estado="Vencido";
            $pago->socio->save();
        }

        return redirect(route('pagos.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pago  $pago
     * @return \Illuminate\Http\Response
     */
    public function destroy($idPago) {
        $socio = Pago::find($idPago)->socio;
        Pago::find($idPago)->delete();
        
        $ultimoPago = Pago::where('fkIdSocio',$socio->idSocio)->orderBy('fechaVencimiento', 'desc')->first();
        if($ultimoPago){
            if($ultimoPago->fechaVencimiento > Carbon::now()){
                $ultimoPago->socio->estado="Al dia";
                $ultimoPago->socio->save();
            }else{
                $ultimoPago->socio->estado="Vencido";
                $ultimoPago->socio->save();
            }
        }else{
            $socio->estado="Vencido";
            $socio->save();
        }
        return redirect(route('pagos.index'));
    }

    public function facturacionAnual(){
        $anio = now()->year;
        $meses = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
        $facturacionAnio = Pago::whereYear('fechaPago', '=', $anio)->get()->sum('monto');
        $facturacionMeses = [];
        $cont = 0;

        foreach($meses as $mes){
            $cont++;
            $facturacionMeses[] = [
                'mes' => $mes,
                'monto' => Pago::whereYear('fechaPago', '=', $anio)->whereMonth('fechaPago', '=', $cont)->get()->sum('monto')
            ];
        }
        //A tener en cuenta: Como esto no es un modelo, para traer el monto de un mes, seria $facturacionMes['monto'], en lugar de $facturacionMes->monto
        
        return view('pagos/facturacionAnual',['facturacionAnio' => $facturacionAnio,'anio' => $anio,'facturacionMeses'=>$facturacionMeses] );
    }
    
    public function graficoFacturacionAnual(){
        $anio = now()->year;
        $meses = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
        $facturacionAnio = Pago::whereYear('fechaPago', '=', $anio)->get()->sum('monto');
        $facturacionMeses = [];
        $cont = 0;

        foreach($meses as $mes){
            $cont++;
            $facturacionMeses[] = [
                'mes' => $mes,
                'monto' => Pago::whereYear('fechaPago', '=', $anio)->whereMonth('fechaPago', '=', $cont)->get()->sum('monto')
            ];
        }
        
        $response = response()->json($facturacionMeses);
        return $response;
    }
}
