<?php

namespace App\Http\Controllers;

use App\Models\Socio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SocioController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        if($request->filtro == 'vencidos'){
            $socios = Socio::estado()->get();
        }
        else{
            $socios = Socio::all();
        }
        
        return view('socios/index',['socios' => $socios]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){ 

        return view('socios/create');  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $socio = new Socio;
        $socio->nombre = $request->nombre;
        $socio->apellido = $request->apellido;
        $socio->dni = $request->dni;
        $socio->direccion = $request->direccion;
        $socio->email = $request->email;
        $socio->estado = "Vencido";
        $socio->save();

        return redirect(route('socios.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Socio  $socio
     * @return \Illuminate\Http\Response
     */
    public function show(Socio $socio){

        return view('socios.show',['socio' => $socio]); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Socio  $socio
     * @return \Illuminate\Http\Response
     */
    public function edit($id){

        $socio = Socio::find($id);

        return view('socios.edit',['socio' => $socio]);  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Socio  $socio
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id){
        $socio = Socio::find($id);
        $socio->nombre = $request->nombre;
        $socio->apellido = $request->apellido;
        $socio->dni = $request->dni;
        $socio->direccion = $request->direccion;
        $socio->email = $request->email;
        $socio->save();

        return redirect(route('socios.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Socio  $socio
     * @return \Illuminate\Http\Response
     */
    public function destroy($idSocio){
        Socio::find($idSocio)->delete();
        
        return redirect(route('socios.index'));
    }
}
