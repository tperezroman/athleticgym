<?php

namespace App\Http\Controllers;

use App\Models\Asistencia;
use App\Models\Pago;
use App\Models\Socio;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class AsistenciaController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        return view('asistencias/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($idSocio){
        $asistencia=Asistencia::find($idSocio);

        return view('asistencias/create',['asitencia' => $asistencia]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $pago = new Pago();
        $asistencia = new Asistencia;
        $fechaHoraAsistencia = Carbon::now()->timezone('America/Argentina/San_Luis');
        $socio = Socio::where('dni', $request->dniAsistencia)->first();
        
        if ($socio != null) {
            $pago = Pago::where('fkIdSocio', $socio->idSocio)->orderBy("idPago","desc")->first();
            
            if ($pago != null) {
                $asistencia->fkIdSocio=$socio->idSocio;
                $asistencia->fechaHoraAsistencia=$fechaHoraAsistencia;
                $asistencia->save();
                $response = response()->json($pago);
            }
            else{
                $response = response()->json(["status" => "error"]);
            }
        }
        else{
            $response = response()->json(["status" => "error"]);
        }

        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Asistencia  $asistencia
     * @return \Illuminate\Http\Response
     */
    public function show(Asistencia $asistencia){
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Asistencia  $asistencia
     * @return \Illuminate\Http\Response
     */
    public function edit($idAsistencia){
        $asistencia=Asistencia::find($idAsistencia);

        return view('asistencias/edit',['asistencia'=>$asistencia]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Asistencia  $asistencia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idAsistencia){
        $asistencia = Asistencia::find($idAsistencia);
        $asistencia->fechaHoraAsistencia=$request->fechaHoraAsistencia;

        $asistencia->save();

        return redirect(route('asistencias.index')); //Ver donde redireccionar
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Asistencia  $asistencia
     * @return \Illuminate\Http\Response
     */
    public function destroy(Asistencia $idAsistencia){
        Asistencia::find($idAsistencia)->delete();

        return redirect(route('asistencias.index'));
    }
}
