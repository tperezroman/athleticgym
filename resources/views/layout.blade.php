<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" href={{ asset('img/favicon.png') }}>
    <title>Gym - @yield('title')</title>

    <!-- Bootstrap en linea -->
    <link rel="icon" href="">
    <link rel="stylesheet" href="{{ asset('css/layout.css') }}">
    <!-- Usa Framework Boostrap en linea -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    {{-- JQuery en linea --}}
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    {{-- Data table css--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
    {{-- Para que el datatable sea responsive en cel --}}
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.bootstrap4.min.css">
    {{-- SweetAlert --}}
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script src="https://cdn.jsdelivr.net/npm/admin-lte@3.1/dist/js/adminlte.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/admin-lte@3.1/dist/css/adminlte.min.css">

</head>
<body class="bg-light">
        <div class="d-flex" id="wrapper">
            <div class="container-fluid">
                <div class="row">
                    <!-- Sidebar -->
                    <div class="col-md-2" id="sidebar">
                        <div class="list-group panel">
                            <a href={{ route('dashboard') }} class="navbar-brand"><img src={{ asset('img/athleticGym.png') }} alt="Logo Athletic Gym" style="width: 10rem"></a>

                            <a href="#menuSocios" class="list-group-item collapsed" data-toggle="collapse" data-parent="#sidebar" aria-expanded="false"><i class="fas fa-user"></i> <span class="hidden-sm-down">Socios</span> </a>
                            <div class="collapse" id="menuSocios">
                                <a href="{{route('socios.index')}}" class="list-group-item" data-parent="#menuSocios">Listado Socios </a>
                                <a href="{{route('socios.create')}}" class="list-group-item" data-parent="#menuSocios">Agregar Socio </a>
                                <a href="{{route('asistencias.index')}}" class="list-group-item" data-parent="#menuSocios">Asistencias </a>
                            </div>

                            <a href="#menuPagos" class="list-group-item collapsed" data-toggle="collapse" data-parent="#sidebar" aria-expanded="false"><i class="fas fa-dollar-sign"></i> <span class="hidden-sm-down">Pagos</span> </a>
                            <div class="collapse" id="menuPagos">
                                <a href="{{route('pagos.index')}}" class="list-group-item" data-parent="#menuPagos">Listado Pagos </a>
                            </div>

                            <a href="{{route('logout')}}" class="list-group-item collapsed"><i class="fas fa-sign-out-alt"></i> Cerrar Sesión</a>
                        </div>
                    </div>
                        <!-- /#sidebar-wrapper -->
                        <!-- Page Content -->
                    <div class="col-10 d-flex" id="page-content-wrapper">
                        <div class="container-fluid p-md-5 d-flex">
                            @yield('content')
                        </div>
                    </div>
                    <!-- /#page-content-wrapper -->
                </div>
            </div>
        </div>
        <!-- /#wrapper -->
      
        <!-- Bootstrap core JavaScript -->
        <!-- then Popper.js, then Bootstrap JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
        {{-- DataTable JS --}}
        <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
        {{-- Para que sea responsive en celu --}}
        <script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.7/js/responsive.bootstrap4.min.js"></script>
</body>
</html>