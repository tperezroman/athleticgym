@extends('layout')

@section('title','Asistencias')

@section('content')
<div class="container">
    <div class="row text-center">
        <div class="col">
            <h2>Asistencias</h2>
        </div>
    </div>

    <div class="container">
        {{-- prueba ajax --}}
        <label for="inputDni">Ingresa tu dni para fichar</label>
        <input type="text" class="form-control" name="dniAsistencia" id="dniAsistencia">
        <div class="mt-4 row">
            <button id="btn1" class="btn btn-success" >Ok</button>
            <div id="error" class="ml-4"></div>
        </div>

    </div>
</div>

<script>
    $(document).ready(function () {

        $("#btn1").click(function () {
            var dni = $("#dniAsistencia").val();

            $.ajax(
            {
                url: "{{route('asistencias.store') }}",
                type: "POST",
                data: {
                    // _token: '{{ csrf_token() }}',
                    dniAsistencia: dni
                },
                error: function(){
                    $("#error").html("<h3>Error de conexión con la base de datos</h3>");
                },
                success: function (response) {
                    $("#error").html("");
                    if(response.status == "error"){
                        Swal.fire({
                            title: 'Error',
                            text: 'El dni ingresado no tiene ningun pago registrado',
                            icon: 'error',
                            // showConfirmButton: false,
                            // timer: 2000
                        });
                    }else{
                        Swal.fire({
                            title: 'Asistencia registrada',
                            text: 'Vencimiento: ' + response.fechaVencimiento,
                            icon: 'success',
                            // showConfirmButton: false,
                            // timer: 2000
                        });
                    }
                }
            });
        });
        
    });
</script>
@endsection