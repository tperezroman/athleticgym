<div class="row">
    <h2 class="text-center">{{$form_action}} socio</h2>
</div>
<div class="row">
        <!-- Nombre -->
        <div class="col">
            <label for="titulo">Nombre</label>
            <div class="my-2 control">
                <input class="form-control" type="input @error('titulo') is-danger @enderror" 
                    type="text"
                    name="nombre" 
                    id="nombre"
                    required
                    @if(isset($socio))
                        value="{{$socio->nombre}}"
                    @else
                        value={{old('nombre')}}
                    @endif
                    
                >
                @error('titulo')
                    <p class="help is-danger">{{$errors->first('titulo')}}</p>
                @enderror
            </div>
        </div>
        <!-- Apellido -->
        <div class="col">
            <label for="titulo">Apellido</label>
            <div class="my-2 control">
                <input class="form-control" type="input @error('titulo') is-danger @enderror" 
                    type="text"
                    name="apellido" 
                    id="apellido"
                    required
                    @if(isset($socio))
                        value="{{$socio->apellido}}"
                    @else
                        value={{old('apellido')}}
                    @endif
                >
                @error('titulo')
                    <p class="help is-danger">{{$errors->first('apellido')}}</p>
                @enderror
            </div>
        </div>
    </div>
    <!-- DNI -->
    <div class="row">
        <div class="col-6">
            <label for="titulo">DNI</label>
            <div class="my-2 control">
                <input class="form-control" type="input @error('titulo') is-danger @enderror" 
                    type="text"
                    name="dni" 
                    id="dni"
                    required
                    @if(isset($socio))
                        value="{{$socio->dni}}"
                    @else
                        value={{old('dni')}}
                    @endif
                >
                @error('titulo')
                    <p class="help is-danger">{{$errors->first('titulo')}}</p>
                @enderror
            </div>
        </div>
    </div>
    <!-- EMAIL -->
    <div class="row">
        <div class="col-6">
            <label for="titulo">Email</label>
            <div class="my-2 control">
                <input class="form-control" type="input @error('titulo') is-danger @enderror" 
                    type="text"
                    name="email" 
                    id="email"
                    @if(isset($socio))
                        value="{{$socio->email}}"
                    @else
                        value={{old('email')}}
                    @endif
                >
                @error('titulo')
                    <p class="help is-danger">{{$errors->first('titulo')}}</p>
                @enderror
            </div>
        </div>
    </div>
    <!-- DIRECCION -->
    <div class="row">
        <div class="col-6">
            <label for="titulo">Direccion</label>
            <div class="my-2 control">
                <input class="form-control" type="input @error('titulo') is-danger @enderror" 
                    type="text"
                    name="direccion" 
                    id="direccion"
                    @if(isset($socio))
                        value="{{$socio->direccion}}"
                    @else
                        value={{old('direccion')}}
                    @endif
                >
                @error('titulo')
                    <p class="help is-danger">{{$errors->first('titulo')}}</p>
                @enderror
            </div>
        </div>
    </div>
