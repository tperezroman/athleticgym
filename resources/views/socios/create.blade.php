@extends('layout')

@section('title','Crear Socio')

@section('content')

    <!-- Main -->
        @php
        $form_action = "Crear"
        @endphp    
        <div class="container">
            <!-- Form -->
            <form method="POST" action="{{route('socios.store')}}">
                @csrf
                @include('socios.form')
                <div class="mt-4">
                    <a href={{route('socios.index')}} class="btn btn-primary">Volver</a>
                    <button class="btn btn-success" type="submit">Crear</button>
                </div>
            </form>
        </div>
@endsection