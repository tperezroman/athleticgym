@extends('layout')

@section('title','Editar Socio')

@section('content')

    <!-- Main -->
        @php
        $form_action = "Editar"
        @endphp    
        <div class="container">
            <!-- Form -->
            <form method="POST" action="{{ route('socios.update',$socio->idSocio) }}" >
                @csrf
                {{method_field('PATCH')}}
                @include('socios.form')
                <div class="mt-4">
                    <a href={{route('socios.index')}} class="btn btn-primary">Volver</a>
                    <button class="btn btn-success" type="submit">Editar</button>
                </div>
            </form>
        </div>
@endsection


