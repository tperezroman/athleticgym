@extends('layout')

@section('title','Socios')

@section('content')
<div class="container">
    <div class="row text-center">
        <div class="col">
            <h2>Listado de Socios</h2>
        </div>
    </div>
    {{-- <div class="row">

        <form method="GET" action="{{ route('socios.index') }}">
            <input type="hidden" value="vencidos" name="filtro">
            <div class="my-4 mx-2">
                <button class="btn btn-success" type="submit">Filtrar Vencidos</button>
            </div>
        </form>

        <div class="mt-4">
            <a href={{route('socios.index')}} class="btn btn-primary">Mostrar Todos</a>
        </div>
    </div> --}}

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <table class="table table-bordered table-striped" id="tablaSocios">
                        </div>
                        <thead>
                            <tr>
                                <th class="filtroSocio">Id Socio</th>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>DNI</th>
                                <th>Estado</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($socios as $socio)
                            <tr>
                                <td>{{$socio->idSocio}}</td>
                                <td>{{$socio->nombre}} </td>
                                <td>{{$socio->apellido}}</td>
                                <td>{{$socio->dni}}</td>
                                <td>
                                    <div class="container">
                                        @if ($socio->estado == 'Vencido')
                                            <button class="btn btn-danger w-100" disabled>
                                                {{$socio->estado}}
                                            </button>
                                        @else
                                            <button class="btn btn-success w-100" disabled>
                                                {{$socio->estado}}
                                            </button>
                                        @endif
                                    </div>
                                </td>
                                <td>

                                    <a class="mx-2 btn btn-success" title="Cargar Pago" href={{ route('pagos.create',$socio->idSocio) }}> 
                                        <i class="fa fa-dollar-sign fa-1x" aria-hidden="true"></i>
                                    </a>
                                    
                                    <a class="mx-2 btn btn-primary" title="Editar" href={{ route('socios.edit',$socio->idSocio) }}> 
                                        <i class="fa fa-user-edit fa-1x" aria-hidden="true"></i>
                                    </a>
                                    <a class="mx-2 btn btn-danger" title="Eliminar" data-toggle="modal" data-target="#delete_register{{$socio->idSocio}}" type="button">
                                        <i class="fa fa-trash-alt"></i>
                                    </a>
                                </td>
                            </tr>
                            <div id="delete_register{{$socio->idSocio}}" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel2">Eliminar Socio</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <h4>Seguro que desea eliminar a {{$socio->nombre}}?</h4>
                                        </div>
                                        <form action={{ route('socios.delete',$socio->idSocio) }} method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                <button type="submit" class="btn btn-danger">Eliminar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready( function () {
        $('#tablaSocios').DataTable(
            {
                responsive: true,
                autoWidth: false,
                "language": {
                    "lengthMenu": "Mostrar _MENU_ registros",
                    "search":         "Buscar:",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoFiltered":   "(filtrado de _MAX_ registros)",
                    "zeroRecords":    "No se encontraron registros",
                    "paginate": {
                        "first":      "Primera",
                        "last":       "Ultima",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                }
            }
        );
    } );
</script>

@endsection



