@extends('layout')

@section('content')

    <!-- Main -->
    <section id="main" class="wrapper style2">
        <div class="container">
            <section id="features">
                <h5>{{$socio->idSocio}} {{$socio->nombre}} {{$socio->apellido}} {{$socio->DNI}}</h5>
            </section>      
        </div>
    </section>
@endsection