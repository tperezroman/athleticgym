@extends('layout')

@section('title','Socios')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col">
                <h2>Panel Informativo</h2>
            </div>
        </div>

        <div class="row">

            <!--<div class="col-4 card text-white bg-dark my-3 w-100">
                <div class="card-header">
                  <h5>Clientes</h5>
                </div>
                <div class="card-body">
                  <h1 class="card-title">{{$cantSocios}}</h1>
                  <p class="card-text"></p>
                  <a href={{ route('socios.index') }} class="btn btn-warning">Detalles</a>
                </div>
            </div>
            
            <div class="col-4 card text-white bg-dark my-3 w-100 ml-3">
                <div class="card-header">
                  <h5>Facturacion Mayo 2021</h5>
                </div>
                <div class="card-body">
                  <h1 class="card-title">${{$facturacionMes}}</h1>
                  <p class="card-text"></p>
                  <a href={{ route('pagos.index') }} class="btn btn-warning">Detalles</a>
                </div>
            </div> -->

            <div class="col-12 col-sm-12 col-lg-4">
              <div class="small-box bg-info">
                <div class="inner">
                  <h1>{{$cantSocios}}</h1>
                  <p class="text-uppercase">Clientes</p>
                </div>
                <div class="icon">
                  <i class="fas fa-users"></i>
                </div>
                <a href={{ route('socios.index') }} class="small-box-footer">Más información <i class="fa fa-arrow-circle-right"></i></a>

              </div>
            </div>

            <div class="col-12 col-sm-12 col-lg-4">
              <div class="small-box bg-primary">
                <div class="inner">
                  <h1>${{$facturacionMes}}</h1>
                  <p class="text-uppercase">{{$mes}} {{$anio}}</p>
                </div>
                <div class="icon">
                  <i class="fas fa-dollar-sign"></i>
                </div>
                <a href={{ route('pagos.index') }} class="small-box-footer"> Ver últimos pagos <i class="fa fa-arrow-circle-right"></i></a>

              </div>
            </div>

            <div class="col-12 col-sm-12 col-lg-4">
              <div class="small-box bg-purple">
                <div class="inner">
                  <h1>${{$facturacionAnio}}</h1>
                  <p class="text-uppercase">Facturación año {{$anio}}</p>
                </div>
                <div class="icon">
                  <i class="fas fa-dollar-sign"></i>
                </div>
                <a href={{ route('pagos.facturacionAnual') }} class="small-box-footer">Más información <i class="fa fa-arrow-circle-right"></i></a>

              </div>
            </div>

            <div class="container">
              <div class="row">
                <div class="col">
                    <a href={{route('asistencias.index')}}> <h4>Asistencias</h4> </a>
                </div>
              </div>

              <div class="card">
                <div class="card-body">
        
                    <div class="row">
                        <div class="col">
                            <table class="table table-bordered table-striped" id="tablaSocios">
                                </div>
                                <thead>
                                    <tr>
                                        <th>Id Asistencia</th>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>Fecha y Hora</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($asistencias as $asistencia)
                                    <tr>
                                        <td>{{$asistencia->idAsistencia}} </td>
                                        <td>{{$asistencia->socio->nombre}}</td>
                                        <td>{{$asistencia->socio->apellido}}</td>
                                        <td>{{date('d/m/Y h:i',strtotime($asistencia->fechaHoraAsistencia))}}</td>
                                    </tr>
                                    
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
    

    <script>
      $(document).ready( function () {
          $('#tablaSocios').DataTable(
              {
                  responsive: true,
                  autoWidth: false,
                  "language": {
                      "lengthMenu": "Mostrar _MENU_ registros",
                      "search":         "Buscar:",
                      "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                      "infoFiltered":   "(filtrado de _MAX_ registros)",
                      "zeroRecords":    "No se encontraron registros",
                      "paginate": {
                          "first":      "Primera",
                          "last":       "Ultima",
                          "next":       "Siguiente",
                          "previous":   "Anterior"
                      },
                  },
                  "order": [[ 0, "desc" ]]
              }
          );
      } );
  </script>
@endsection