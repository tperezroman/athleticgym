@extends('layout')

@section('title','Pagos')

@section('content')
<div class="container">
    <div class="row text-center">
        <div class="col">
            <h2>Listado de Pagos</h2>
        </div>
    </div>

    <div class="card">
        <div class="card-body">

            <div class="row">
                <div class="col">
                    <table class="table table-bordered table-striped" id="tablaPagos">
                        </div>
                        <thead>
                            <tr>
                                <th>Id Pago</th>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Monto</th>
                                <th>Fecha de Pago</th>
                                <th>Fecha de Vencimiento</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($pagos as $pago)
                            <tr>
                                <td>{{$pago->idPago}} </td>
                                <td>{{$pago->socio->nombre}}</td>
                                <td>{{$pago->socio->apellido}}</td>
                                <td>{{$pago->monto}}</td>
                                <td>{{date('d/m/Y', strtotime($pago->fechaPago))}}</td>
                                <td>{{date('d/m/Y',strtotime($pago->fechaVencimiento))}}</td>
                                <td>
                                    <a title="Editar Pago" style="text-decoration:none" href={{route('pagos.edit',$pago->idPago) }}> 
                                        <button class="btn btn-primary"><i class="fa fa-edit fa-1x" aria-hidden="true"></i></button>
                                    </a>
                                    <a title="Eliminar Pago" class="mx-2 btn btn-danger" title="Eliminar" data-toggle="modal" data-target="#delete_register{{$pago->idPago}}" type="button">
                                        <i class="fa fa-trash-alt"></i>
                                    </a>
                                </td>
                            </tr>
                            <div id="delete_register{{$pago->idPago}}" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel2">Eliminar Pago</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <h4>Seguro que desea eliminar este pago?</h4>
                                        </div>
                                        <form action={{ route('pagos.delete',$pago->idPago) }} method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                <button type="submit" class="btn btn-danger">Eliminar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready( function () {
        $('#tablaPagos').DataTable(
            {
                responsive: true,
                autoWidth: false,                
                "language": {
                    "lengthMenu": "Mostrar _MENU_ registros",
                    "search":         "Buscar:",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoFiltered":   "(filtrado de _MAX_ registros)",
                    "zeroRecords":    "No se encontraron registros",
                    "paginate": {
                        "first":      "Primera",
                        "last":       "Ultima",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                },
                "order": [[ 5, "desc" ]]
            }
        );
    } );
</script>
    
@endsection