@extends('layout')

@section('title','Agregar Pago')

@section('content')

        <!-- Form -->
        <form method="POST" action="{{route('pagos.store')}}">
            @csrf

            <div class="row">
                <h2 class="text-center">Cargar pago a: {{$socio->nombre}} {{$socio->apellido}}</h2>
            </div>
            
            @include('pagos.form')
            
            <div class="field is-grouped">
                <div class="control">
                    <a href={{route('socios.index')}} class="btn btn-primary">Volver</a>
                    <button class="btn btn-success" type="submit">Registrar</button>
                </div>
            </div>
        </form>

        <script>

        $(document).ready(function () {
            $('#fechaPago').val(new Date().toDateInputValue());
            $('#fechaVencimiento').val(new Date().setMonth());
        });
        
        </script>
@endsection