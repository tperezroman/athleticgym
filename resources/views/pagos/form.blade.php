    <!-- Id -->
    <div class="row">
        <div class="col">
            <div class="my-2 control">
                    <input class="form-control" type="hidden" 
                        type="number"
                        name="fkIdSocio" 
                        id="fkIdSocio"
                        required
                        @if(isset($pago))             
                            value="{{$pago->fkIdSocio}}"
                        @else
                            value={{$socio->idSocio}}
                        @endif
                    >
                    @error('titulo')
                        <p class="help is-danger">{{$errors->first('fkIdSocio')}}</p>
                    @enderror
            </div>
        </div>
    </div>
        <!-- Monto -->
        <div class="row">
            <div class="col">
                <label for="titulo">Monto</label>
                <div class="my-2 control">
                    <input min="1" max="5000" class="form-control" 
                        type="number"
                        name="monto" 
                        id="monto"
                        required
                        @if(isset($pago))
                            value="{{$pago->monto}}"
                        @else
                            value={{old('monto')}}
                        @endif
                    >
                    @error('titulo')
                        <p class="help is-danger">{{$errors->first('monto')}}</p>
                    @enderror
                </div>
            </div>
        </div>
    <!-- Fecha de pago -->
    <div class="row">
        <div class="col">
            <label for="titulo">Fecha de pago</label>
            <div class="my-2 control">
                <input class="form-control" 
                    type="date"
                    name="fechaPago" 
                    id="fechaPago"
                    required
                    @if(isset($pago))
                    value="{{$pago->fechaPago}}"
                    @else
                        value={{old('fecha')}}
                    @endif
                >
                @error('titulo')
                    <p class="help is-danger">{{$errors->first('fecha')}}</p>
                @enderror
            </div>
        </div>
    </div>
    <!-- Fecha vencimiento -->
    <div class="row">
        <div class="col">
            <label for="titulo">Fecha Vencimiento</label>
            <div class="my-2 control">
                <input class="form-control"
                    type="date"
                    name="fechaVencimiento" 
                    id="fechaVencimiento"
                    @if(isset($pago))
                        value="{{$pago->fechaVencimiento}}"
                    @else
                        value={{old('fechaVencimiento')}}
                    @endif
                >
                @error('titulo')
                    <p class="help is-danger">{{$errors->first('fechaVencimiento')}}</p>
                @enderror
            </div>
        </div>
    </div>

    <script>
        Date.prototype.toDateInputValue = (function(){
            var local = new Date(this);
            console.log(local);
            return local.toJSON().slice(0,10);
        });

        Date.prototype.setMonth = (function(){
            var fecha, mesVencimiento, fechaVencimiento;
            fecha = new Date(this);//fecha actual
            mesVencimiento = fecha.getMonth() + 1;

            fechaVencimiento = new Date(fecha.getFullYear(),mesVencimiento,fecha.getDate());
            console.log(fechaVencimiento);

            return fechaVencimiento.toJSON().slice(0,10);
        });


        $('#fechaPago').change(
            function(){
                var fechaPago, fechaVencimiento;
                fechaPago = new Date($('#fechaPago').val());
                fechaVencimiento = new Date(fechaPago.getFullYear(), fechaPago.getMonth()+1, fechaPago.getDate());
                $('#fechaVencimiento').val(fechaVencimiento.toJSON().slice(0,10)); 
            }
        )
    </script>
        
    
