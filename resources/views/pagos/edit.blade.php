@extends('layout')

@section('title','Editar Pagos')

@section('content')

        <!-- Form -->
        <form method="POST" action="{{ route('pagos.update',$pago->idPago) }}">
            @csrf
            {{method_field('PATCH')}}

            <div class="row">
                <h2 class="text-center">Editar pago de: {{$pago->socio->nombre}} {{$pago->socio->apellido}}</h2>
            </div>
            @include('pagos.form')
            <div class="field is-grouped">
                <div class="control">
                    <a href={{route('pagos.index')}} class="btn btn-primary">Volver</a>
                    <button class="btn btn-success" type="submit">Editar</button>
                </div>
            </div>
        </form>

@endsection