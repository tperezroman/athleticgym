@extends('layout')

@section('title','Factuacion anual')

@section('content')

    {{-- <div class="col-12 col-sm-12 col-lg-4"> --}}
        <div class="col">
        <div class="small-box bg-purple">
            <div class="inner">
                <h1>${{$facturacionAnio}}</h1>
                <p class="text-uppercase">Facturación año {{$anio}}</p>
            </div>
            <div class="icon">
                <i class="fas fa-dollar-sign"></i>
            </div>
        </div>
        <div class="row">
        <div class="col">
            <canvas id="myChart"></canvas>
        </div>
        </div>
    </div>

    {{-- Chart.Js --}}
    <script src="https://cdn.jsdelivr.net/npm/chart.js@3.4.0/dist/chart.min.js"></script>

    <script>
        $(document).ready(function () {
            var meses = [];
            var precios = [];
            $.ajax(
            {
                url: "{{route('pagos.graficoFacturacionAnual') }}",
                type: "GET",
                success: function (response) {
                    //cargo los arreglos con la respuesta del ajax
                    response.forEach(element => {
                        meses.push(element.mes);
                        precios.push(element.monto);
                    });
                    //armo el grafico
                    var ctx = document.getElementById('myChart').getContext('2d');
                    var myChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: meses,
                            datasets: [{
                                label: 'Facturación mensual',
                                data: precios,
                                backgroundColor: [
                                    'rgba(0, 0, 0, 0.5)',
                                    'rgba(0, 0, 0, 0.5)',
                                    'rgba(0, 0, 0, 0.5)',
                                    'rgba(0, 0, 0, 0.5)',
                                    'rgba(0, 0, 0, 0.5)',
                                    'rgba(0, 0, 0, 0.5)',
                                    'rgba(0, 0, 0, 0.5)',
                                    'rgba(0, 0, 0, 0.5)',
                                    'rgba(0, 0, 0, 0.5)',
                                    'rgba(0, 0, 0, 0.5)',
                                    'rgba(0, 0, 0, 0.5)',
                                    'rgba(0, 0, 0, 0.5)',
                                ],
                                borderColor: [
                                    'rgba(0, 0, 0, 1)',
                                    'rgba(0, 0, 0, 1)',
                                    'rgba(0, 0, 0, 1)',
                                    'rgba(0, 0, 0, 1)',
                                    'rgba(0, 0, 0, 1)',
                                    'rgba(0, 0, 0, 1)',
                                    'rgba(0, 0, 0, 1)',
                                    'rgba(0, 0, 0, 1)',
                                    'rgba(0, 0, 0, 1)',
                                    'rgba(0, 0, 0, 1)',
                                    'rgba(0, 0, 0, 1)',
                                    'rgba(0, 0, 0, 1)',
                                ],
                                borderWidth: 1
                            }]
                        },
                        options: {
                            scales: {
                                y: {
                                    beginAtZero: true
                                }
                            }
                        }
                    });
                }
            });
        });


        </script>
        
@endsection