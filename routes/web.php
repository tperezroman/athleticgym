<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\SocioController;
use App\Http\Controllers\PagoController;
use App\Http\Controllers\AsistenciaController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/inicio',[HomeController::class, 'index'])->name('dashboard')->middleware('auth');
require __DIR__.'/auth.php';


//Rutas para Socios

// Muestra todos los socios
Route::get('/socios',[SocioController::class, 'index'])->name('socios.index')->middleware('auth');
//Muestra un formulario para crear un socio
Route::get('/socios/agregar',[SocioController::class, 'create'])->name('socios.create')->middleware('auth');
//Guarda la creacion de un socio
Route::post('/socios/crear',[SocioController::class, 'store'])->name('socios.store')->middleware('auth');
// Muestra un socio
Route::get('/socios/{socio}',[SocioController::class, 'show'])->name('socios.show')->middleware('auth');
// Muestra un formulario para editar un socio
Route::get('/socios/{socio}/editar',[SocioController::class, 'edit'])->name('socios.edit')->middleware('auth');
// Guarda la modificacion de un socio
Route::patch('/socios/{socio}',[SocioController::class, 'update'])->name('socios.update')->middleware('auth');
// Elimina un socio
Route::delete('/socios/{socio}',[SocioController::class, 'destroy'])->name('socios.delete')->middleware('auth');

//Rutas para Pagos

// Muestra todos los pagos
Route::get('/pagos',[PagoController::class, 'index'])->name('pagos.index')->middleware('auth');
//Muestra un formulario para cargar un pago a un socio
Route::get('/pagos/{socio}/agregar',[PagoController::class, 'create'])->name('pagos.create')->middleware('auth');
//Guarda la creacion de un pago
Route::post('/pagos/crear',[PagoController::class, 'store'])->name('pagos.store')->middleware('auth');
// Muestra un formulario para editar un pago
Route::get('/pagos/{pagos}/editar',[PagoController::class, 'edit'])->name('pagos.edit')->middleware('auth');
// Guarda la modificacion de un pago
Route::patch('/pagos/{pagos}',[PagoController::class, 'update'])->name('pagos.update')->middleware('auth');
// Elimina un pago
Route::delete('/pagos/{pagos}',[PagoController::class, 'destroy'])->name('pagos.delete')->middleware('auth');
Route::get('/pagos/facturacionAnual',[PagoController::class, 'facturacionAnual'])->name('pagos.facturacionAnual')->middleware('auth');
Route::get('/pagos/graficoFacturacionAnual',[PagoController::class, 'graficoFacturacionAnual'])->name('pagos.graficoFacturacionAnual')->middleware('auth');

//Rutas para Asistencia

// Muestra todas las Asistencias
Route::get('/asistencias',[AsistenciaController::class, 'index'])->name('asistencias.index');
// Muestra modal de carga de asistencia
Route::get('/asistencias/{socio}/agregar',[AsistenciaController::class, 'create'])->name('asistencias.create')->middleware('auth');
// Guarda la creacion de una asistencia
Route::post('/asistencias/crear',[AsistenciaController::class, 'store'])->name('asistencias.store')->middleware('auth');
// Muestra un formulario para editar una asistencia
Route::get('/asistencias/{asistencias}/editar',[AsistenciaController::class, 'edit'])->name('asistencias.edit')->middleware('auth');
// Guarda la modificacion de un asistencia
Route::patch('/asistencias/{asistencias}',[AsistenciaController::class, 'update'])->name('asistencias.update')->middleware('auth');
// Elimina un asistencia
Route::delete('/asistencias/{asistencias}',[AsistenciaController::class, 'destroy'])->name('asistencias.delete')->middleware('auth');